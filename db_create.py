#!/usr/bin/env python2
# coding=utf-8
import os.path
from migrate.versioning import api
from app.settings import SQLALCHEMY_DATABASE_URI
from app.settings import SQLALCHEMY_MIGRATE_REPO
from app import db
from app.models.param import Param
from app.models.category import Category
from app.models.shipping import Shipping
from app.models.discount import Discount
from app.models.product_param import ProductParam
from app.models.gallery import Gallery, Img

db.create_all()

param1 = Param(name='sizes')
param2 = Param(name='colors')
category1 = Category(name=u'dziewczynka')
category2 = Category(name=u'chłopiec')
shipping = Shipping(title=u'Przesyłka kurierska', value=17.50)
shipping2 = Shipping(title=u'Przesyłka kurierska pobraniowa',
                     value=22.50, cash_on_delivery=True)
discount = Discount(value=250, discount=3)
discount2 = Discount(value=350, discount=5)
discount3 = Discount(value=550, discount=7)

db.session.add_all([
    param1, param2, category1, category2, shipping, shipping2,
    discount, discount2, discount3
])
db.session.commit()

if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
    api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
else:
    api.version_control(
        SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO,
        api.version(SQLALCHEMY_MIGRATE_REPO))
