from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
product = Table('product', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=150)),
    Column('cost', Float),
    Column('description', Text),
)

shipping = Table('shipping', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('title', String(length=90)),
    Column('cash_on_delivery', Boolean, default=ColumnDefault(False)),
    Column('value', Float(precision=2)),
    Column('is_active', Boolean, default=ColumnDefault(True)),
)

site = Table('site', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('title', String(length=90)),
    Column('link_name', String(length=40)),
    Column('keywords', String(length=255)),
    Column('description', String(length=255)),
    Column('content', Text),
    Column('main_site', Boolean, default=ColumnDefault(False)),
)

user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('nickname', String(length=64)),
    Column('email', String(length=120)),
    Column('role', SmallInteger, default=ColumnDefault(0)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['product'].create()
    post_meta.tables['shipping'].create()
    post_meta.tables['site'].create()
    post_meta.tables['user'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['product'].drop()
    post_meta.tables['shipping'].drop()
    post_meta.tables['site'].drop()
    post_meta.tables['user'].drop()
