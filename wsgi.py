# -*- coding: utf-8 -*-
"""
    wsgi
    ~~~~

    Deploy with apache2 wsgi.
"""

import os
import sys
import pwd

os.environ['FBONE_APP_CONFIG'] = ''
os.environ['HOME'] = pwd.getpwuid(os.getuid()).pw_dir

activate_this = '/home/diltram/.envs/app/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

BASE_DIR = os.path.join(os.path.dirname(__file__))
if BASE_DIR not in sys.path:
    sys.path.append(BASE_DIR)

from app import create_app
application = create_app()
