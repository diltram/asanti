import os

DEBUG = True
BASEDIR = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = 'temporary_secret_key'  # make sure to change this
UPLOAD_FOLDER = os.path.join(BASEDIR, 'static', 'galleries')

SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/angular_flask.db'
SQLALCHEMY_MIGRATE_REPO = os.path.join(BASEDIR, '..', 'db_repository')
