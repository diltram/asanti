function editPool () {
    $("input.edit").each(function(){
        $(this).click(function(){
            var thisButtonId = $(this).attr("id");
            var id = $(this).attr("id");
            id = id.substr(5,10);

            var val = $("input#cat_" + id).val();
            $.ajax({
                type: 'POST',
                url: '/api/admin/categories',
                data: {action : "change", categoryId: id, categoryName: val},

                error: function (data) {
                    alert("Nie udało się zmienić nazwy kategori");
                },
                success: function (data) {
                    if("OK" == data) {
                        $("#confirmAlert").fadeIn("fast");
                        $("#confirmAlert").delay(800).fadeOut(800);
                    } else {
                        alert("Nie udało się zmienić nazwy kategori");
                    }
                },
            });
        });
    })
}

function addCategory(){
    $("input.add").each(function(){
        $(this).click(function(){
            var id = $(this).attr("id");
            id = id.substr(4,10);
            var val = $("input#newCategory_" + id).val();
            var parentId = id;
            $.ajax({
                type: 'POST',
                url: '/api/admin/categories',
                data: {action : "add", categoryName : val, parentId : parentId},

                error: function (data) {
                    alert("Nie udało się dodać nowej kategori");
                },
                success: function (data) {
                    if("OK" == data) {
                        window.location.replace(window.location.href);
                    } else {
                        alert("Nie udało się zmienić nazwy kategori");
                    }
                }
            });
        });
    });
}

function deleteCategory(){
    $("input.delete").each(function(){
        $(this).click(function(){
            var id = $(this).attr("id");
            id = id.substr(7,10);

            $.ajax({
                type: 'POST',
                url: '/api/admin/categories',
                data: {action : "delete", categoryId : id},

                error: function (data) {
                    alert("Nie udało się skasować kategori");
                },
                success: function (data) {
                    if("OK" == data) {
                        window.location.replace(window.location.href);
                    }else{
                        alert("Nie udało się skasować kategori");
                    }

                },
            });
        })
    })
}

function changeActiveCategory(){
    var active;
    $(".squaredOneCheckbox").change(function(){
        if($(this).is(':checked')){
            active = 1;
        }else{
            active = 0;
        }
        var categoryId = $(this).attr("id");
        categoryId = categoryId.substr(10,5);
        $.ajax({
            type: 'POST',
            url: '/api/admin/categories',
            data: {action : "changeActive", categoryId: categoryId, active: active},
            error: function (data) {
                alert("Nie udało się zmienić stanu kategori");
            },
            success: function (data) {
                if("OK" != data) {
                    alert("Nie udało się zmienić stanu kategori");
                }
            },
        })
    });
}

function editParamOfPool(){
    $("input.edit").each(function(){
        $(this).click(function(){
            var id = $(this).attr("id").substr(5,10);
            var val = $("input#value_" + id).val();
            $.ajax({
                type: 'POST',
                url: '/api/admin/params',
                data: {action : "changeParam", id: id, value: val},

                error: function (data) {
                    alert("Nie udało się zmienić parametru");
                },
                success: function (data) {
                    window.location.replace(window.location.href);
                },
            });
        });
    })
}

function addNewParamValue(){
    $("input.add").each(function(){
        $(this).click(function(){
            var paramOf = $(this).attr("id").substr(4, 30);
            var val = $("input#newSize_" + paramOf).val();
            $.ajax({
                type: 'POST',
                url: '/api/admin/params',
                data: {action : "addValue", value: val, paramOf: paramOf},

                error: function (data) {
                    alert("Nie udało się dodać parametru");
                },
                success: function (data) {
                    window.location.replace(window.location.href);
                },
            });
        });
    });
}

function deleteParam(){
    $("input.delete").each( function () {
        $(this).click(function(){
            var id = $(this).attr("id");
            id = id.substr(7,10);
            $.ajax({
                type: 'POST',
                url: '/api/admin/params',
                data: {action : "delete", paramId : id},

                error: function (data) {
                    alert("Nie udało się usunąć parametru");
                },
                success: function (data) {
                    window.location.replace(window.location.href);
                },
            });
        })
    })
}

function addNewParamGroup () {
    $('input[name="addNew"]').click( function () {
        var name = $('input[name="name"]').val();
        var value = $('input[name="value"]').val();
        $.ajax({
            type: 'POST',
            url: '/api/admin/params',
            data: {action : "addNewGroup", name: name, value: value},

            error: function (data) {
                alert("Nie udało się dodać parametru");
            },
            success: function (data) {
                window.location.replace(window.location.href);
            },
        });
    });
}

function updateShipping(){
    $("input[name='submit']").click(function(){
        var shipId = $(this).attr("id").substr(5,5);
        var name = $("input#count_" + shipId).val();
        var value = $("input#disc_" + shipId).val();
        $.ajax({
            type: 'POST',
            url: '/api/admin/shipping',
            data: {action: "update", shipId: shipId, name: name, value: value},
            error: function (data) {
                // alert("error");
            },
            success: function (data) {
                $("#confirmAlert").fadeIn("fast");
                $("#confirmAlert").delay(800).fadeOut(800);
            },
        });
    });
}

function changeActiveShipping(){
    var active;
    $(".squaredOneCheckbox").change(function(){
        if($(this).is(':checked')){
            active = 1;
        }else{
            active = 0;
        }
        var shipId = $(this).attr("id");
        shipId = shipId.substr(10,5);
        $.ajax({
            type: 'POST',
            url: '/api/admin/shipping',
            data: {action: "changeActive", shipId: shipId, active: active},
            error: function (data) {
            },
            success: function (data) {
                $("#confirmAlert").fadeIn("fast");
                $("#confirmAlert").delay(800).fadeOut(800);
            },
        })
    });
}

function deleteShip(){
    $("input[name='delete']").each(function(){
        $(this).click(function(){
            var shipId = $(this).attr("id");
            shipId = shipId.substr(5,10);
            $.ajax({
                type: 'POST',
                url: '/api/admin/shipping',
                data: {action : "delete", shipId : shipId},
                error: function (data) {
                    // alert("porażka!");
                },
                success: function (data) {
                    window.location.replace(window.location.href);
                },
            });
        })
    })
}

function updateDiscount(){
    $("input[name='submit']").click(function(){
        var discId = $(this).attr("id").substr(5,5);
        var count = $("input#count_" + discId).val();
        var disc = $("input#disc_" + discId).val();
        $.ajax({
            type: 'POST',
            url: '/api/admin/discount',
            data: {action: "update", discId: discId, count: count, disc: disc},
            error: function (data) {
                // alert("error");
            },
            success: function (data) {
                $("#confirmAlert").fadeIn("fast");
                $("#confirmAlert").delay(800).fadeOut(800);
            },
        });
    });
}

function changeActiveDiscount(){
    var active;
    $(".squaredOneCheckbox").change(function(){
        if($(this).is(':checked')){
            active = 1;
        }else{
            active = 0;
        }
        var discId = $(this).attr("id");
        discId = discId.substr(10,5);
        $.ajax({
            type: 'POST',
            url: '/api/admin/discount',
            data: {action: "changeActive", discId: discId, active: active},
            error: function (data) {
            },
            success: function (data) {
                $("#confirmAlert").fadeIn("fast");
                $("#confirmAlert").delay(800).fadeOut(800);
            },
        })
    });
}

function showPreview(){
    var inputLocalFont = document.getElementById("image-input");
    inputLocalFont.addEventListener("change",previewImages,false);

    function previewImages(){
        var fileList = this.files;

        var anyWindow = window.URL || window.webkitURL;

        for(var i = 0; i < fileList.length; i++){
            var objectUrl = anyWindow.createObjectURL(fileList[i]);
            $('.preview-area').append('<img src="' + objectUrl + '" style="max-width: 150px; max-height: 150px;"/>');
            window.URL.revokeObjectURL(fileList[i]);
        }
    }
}

function deleteGallery(){
    $("input.delete").click(function(){
        if(!window.confirm("Na pewno chcesz usunąć tę galerię?")){
            return false;
        }else{
            var galleryId = $(this).attr("id");
            galleryId = galleryId.substr(12,5);
            $.ajax({
                type: 'POST',
                url: '/api/admin/gallery',
                data: {action: "delete", galleryId: galleryId},
                timeout: 50000,
                error: function (data) {
                    // alert("ajaxError");
                },
                success: function (data) {
                    window.location.reload(window.location.href);
                },
            });
        }
    });
}

function deletePhoto(){
    $(".deletePhoto").click(function(){
        var photoId = $(this).attr("id").substr(11,10);
        if(!window.confirm("Na pewno chcesz usunąć to zdjęcie?")){
            return false;
        }else{
            $.ajax({
            type: 'POST',
            url: '/api/admin/gallery',
            data: {action: "deletePhoto", photoId: photoId},

            error: function (data) {
                // alert("porażka!");
            },
            success: function (data) {
                window.location.replace(window.location.href);
            },
        })
    }
    })
}

function moveBefore(){
    $(".moveBefore").click(function(){
        var photoId = $(this).attr("id").substr(10,10);
        var direction = "left";

        $.ajax({
            type: 'POST',
            url: '/api/admin/gallery',
            data: {action : "move", photoId: photoId, direction: direction},

            error: function (data) {
                // alert("porażka!");
            },
            success: function (data) {
                window.location.replace(window.location.href);
            },
        });
    });
}

function moveAfter(){
    $(".moveAfter").click(function(){
        var photoId = $(this).attr("id").substr(9,10);
        var direction = "right";
        $.ajax({
            type: 'POST',
            url: '/api/admin/gallery',
            data: {action : "move", photoId: photoId, direction: direction},

            error: function (data) {
                // alert("porażka!");
            },
            success: function (data) {
                window.location.replace(window.location.href);
            },
        });
    });
}

function changeActiveProduct(){
    var active;
    $(".squaredOneCheckbox").change(function(){
        if($(this).is(':checked')){
            active = 1;
        }else{
            active = 0;
        }
        var productId = $(this).attr("id");
        productId = productId.substr(10, 5);
        $.ajax({
            type: 'POST',
            url: '/api/admin/product',
            data: {action: 'changeActive', productId: productId, active: active},
            error: function (data) {
                alert("Nie udało się zmienić stanu produktu");
            },
            success: function (data) {
                if("OK" != data) {
                    alert("Nie udało się zmienić stanu produktu");
                }
            },
        })
    });
}

function deleteProduct (){
    $("input.delete").click(function(){
        if(!window.confirm("Na pewno chcesz usunąć ten produkt?")){
            return false;
        }else{
            var productId = $(this).attr("id");
            productId = productId.substr(12,5);
            $.ajax({
                type: 'POST',
                url: '/api/admin/product',
                data: {action: "delete", productId: productId},
                timeout: 50000,
                error: function (data) {
                    // alert("ajaxError");
                },
                success: function (data) {
                    window.location.reload(window.location.href);
                },
            });
        }
    });
}
