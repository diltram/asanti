'use strict';

var app = angular.module('AsantiApp', [])
    .config(['$routeProvider', '$locationProvider',
            function ($routeProvider, $locationProvider) {
                $routeProvider
                .when('/', {
                    templateUrl: 'partials/main_page.html',
                    controller: IndexController
                })
                .when('/promo', {
                    templateUrl: 'partials/main_page.html',
                    controller: IndexController
                });

                $locationProvider.html5Mode(true);
            }
    ]
);
