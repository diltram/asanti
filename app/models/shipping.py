# coding=utf-8
from app import db


class Shipping(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(90))
    cash_on_delivery = db.Column(db.Boolean, default=False)
    value = db.Column(db.Float(precision=2))
    is_active = db.Column(db.Boolean, default=True)
