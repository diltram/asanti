# coding=utf-8
from app import db


class Discount(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Float(precision=2))
    discount = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=True)
