from app import db


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(90))
    sub_categories = db.relationship('Category', backref=db.backref(
        'parent', remote_side=[id]))
    parent_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    active = db.Column(db.Boolean, default=True)

    def __unicode__(self):
        return u'{}'.format(self.name)

    @classmethod
    def get_main_categories(cls):
        return cls.query.filter_by(parent=None).all()
