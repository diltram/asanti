import os
from werkzeug.utils import secure_filename
from app import db
from app import app


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150))
    cost = db.Column(db.Float)
    description = db.Column(db.Text)
    params = db.relationship('ProductParam')
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship('Category')
    is_active = db.Column(db.Boolean, default=True)
    images = db.relationship('ProductImg', lazy='dynamic', backref=db.backref(
        'parent', remote_side=[id]))

    def main_img(self):
        return self.images.order_by(ProductImg.order).first()

    def upload_images(self, imgs):
        dir_path = os.path.join(
            app.config['UPLOAD_FOLDER'], 'product', str(self.id))
        if not os.path.isdir(dir_path):
            os.mkdir(dir_path, 0764)

        gallery_imgs = []
        inc = self.images.count()
        for index, img in enumerate(imgs):
            index = index + inc
            name = secure_filename(img.filename)
            gallery_imgs.append(ProductImg(
                name=name, parent=self, order=index))
            img.save(os.path.join(dir_path,  name))

        db.session.add_all(gallery_imgs)

    def delete_images(self):
        imgs = self.images.all()
        for img in imgs:
            img.remove_file()
            db.session.delete(img)


class ProductImg(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    order = db.Column(db.Integer)
    parent_id = db.Column(db.Integer, db.ForeignKey('product.id'))

    def url(self):
        return '/product-img/{}/{}'.format(self.parent_id, self.id)

    def remove_file(self):
        file_path = os.path.join(
            app.config['UPLOAD_FOLDER'], 'product', str(self.parent_id),
            self.name)
        os.remove(file_path)
