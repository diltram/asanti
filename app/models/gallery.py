import os
import shutil
from werkzeug.utils import secure_filename
from app import db
from app import app


class Gallery(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    images = db.relationship('Img', lazy='dynamic', backref=db.backref(
        'parent', remote_side=[id]))

    def main_img(self):
        return self.images.order_by(Img.order).first()

    def ordered_imgs(self):
        return self.images.order_by(Img.order).all()

    def upload_images(self, imgs):
        dir_path = os.path.join(app.config['UPLOAD_FOLDER'], str(self.id))
        if not os.path.isdir(dir_path):
            os.mkdir(dir_path, 0764)

        gallery_imgs = []
        inc = self.images.count()
        for index, img in enumerate(imgs):
            index = index + inc
            name = secure_filename(img.filename)
            gallery_imgs.append(Img(name=name, parent=self, order=index))
            img.save(os.path.join(dir_path,  name))

        db.session.add_all(gallery_imgs)
        db.session.commit()

    def delete_images(self):
        imgs = self.images.all()
        for img in imgs:
            db.session.delete(img)

        dir_path = os.path.join(app.config['UPLOAD_FOLDER'], str(self.id))
        shutil.rmtree(dir_path)

        db.session.commit()


class Img(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    order = db.Column(db.Integer)
    parent_id = db.Column(db.Integer, db.ForeignKey('gallery.id'))

    def url(self):
        return '/galleries/{}/{}'.format(self.parent_id, self.name)

    def remove_file(self):
        file_path = os.path.join(
            app.config['UPLOAD_FOLDER'], str(self.parent_id), self.name)
        os.remove(file_path)
