from app import db


class ProductParam(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    param_id = db.Column(db.Integer, db.ForeignKey('param.id'))
