from app import db


class User(db.Model):
    ROLE_USER = 0
    ROLE_ADMIN = 1
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    is_active = db.Column(db.Boolean, default=False)

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.is_active

    def is_anonymous():
        return False

    def get_id(self):
        return u'{}'.format(self.id)

    def is_admin(self):
        return self.role == User.ROLE_ADMIN

    def __repr__(self):
        return '<User %r>' % (self.nickname)
