from app import db


class Param(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    sub_params = db.relationship('Param', backref=db.backref(
        'parent', remote_side=[id]))
    parent_id = db.Column(db.Integer, db.ForeignKey('param.id'))
