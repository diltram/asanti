from app import db


class Site(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(90))
    link_name = db.Column(db.String(40))
    keywords = db.Column(db.String(255))
    description = db.Column(db.String(255))
    content = db.Column(db.Text)
    main_site = db.Column(db.Boolean, default=False)
