from flask import Flask
from flask_login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.restless import APIManager

app = Flask(__name__, static_url_path='')
app.config.from_object('app.settings')
app.url_map.strict_slashes = False

login_manager = LoginManager()
login_manager.init_app(app)

db = SQLAlchemy(app)
api_manager = APIManager(app, flask_sqlalchemy_db=db)

from app.controllers.main import blueprint as main_blueprint
from app.controllers.admin import blueprint as admin_blueprint
from app.controllers.admin_api import blueprint as api_admin_blueprint

app.register_blueprint(main_blueprint, url_prefix='/')
app.register_blueprint(admin_blueprint, url_prefix='/cms')
app.register_blueprint(api_admin_blueprint, url_prefix='/api/admin')
