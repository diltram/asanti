# coding=utf-8
from flask import request
from flask import url_for
from flask import redirect
from flask import Blueprint
from flask import render_template
from flask.ext.login import current_user
from app import db
from app.models.category import Category
from app.models.param import Param
from app.models.shipping import Shipping
from app.models.discount import Discount
from app.models.gallery import Gallery
from app.models.product import Product
from app.models.product_param import ProductParam

blueprint = Blueprint('admin', __name__)


# @blueprint.before_request
def restrict_blueprint_to_admins():
    if not current_user.is_authenticated() or not current_user.is_admin():
        return redirect('/login')  # request.url)


@blueprint.route('/')
def orders(**kwargs):
    return render_template('admin/orders.html')


@blueprint.route('/items')
@blueprint.route('/items/page/<int:page>')
@blueprint.route('/items/order/<order>')
@blueprint.route('/items/page/<int:page>/order/<order>')
def items(page=1, order='name_asc'):
    order = order.replace('_', ' ')
    products = Product.query.order_by(order).paginate(page, per_page=16)
    categories = Category.query.filter(Category.parent == None).all()
    return render_template('admin/items.html', products=products,
                           categories=categories)


@blueprint.route('/items/add', methods=['GET', 'POST'])
def add_item():
    if request.method == 'POST':
        param = request.form
        prod = Product(
            name=param['name'], description=param['description'],
            cost=param['price'], category_id=param['categoryToPost']
        )
        db.session.add(prod)
        db.session.commit()

        for name in ['pickColor[]', 'pickSize[]']:
            for par in param.getlist(name):
                pr_par = ProductParam(product_id=prod.id, param_id=int(par))
                db.session.add(pr_par)

        imgs = request.files.getlist('userfile[]')
        prod.upload_images(imgs)

        db.session.commit()
        return redirect(url_for('admin.items'))

    sizes = Param.query.filter(Param.name == 'sizes').first()
    colors = Param.query.filter(Param.name == 'colors').first()
    categories = Category.query.filter(Category.parent == None).all()
    return render_template('admin/add_item.html', sizes=sizes, colors=colors,
                           categories=categories)


@blueprint.route('/categories')
def categories(**kwargs):
    categories = Category.get_main_categories()
    return render_template('admin/categories.html', categories=categories)


@blueprint.route('/sizes')
def sizes(**kwargs):
    sizes_parent = Param.query.filter_by(name='sizes').first()
    sizes = Param.query.filter_by(parent=sizes_parent).all()
    return render_template('admin/sizes.html', sizes=sizes)


@blueprint.route('/colors')
def colors(**kwargs):
    colors_parent = Param.query.filter_by(name='colors').first()
    colors = Param.query.filter_by(parent=colors_parent).all()
    return render_template(
        'admin/colors.html', colors=colors, parent=colors_parent)


@blueprint.route('/connections')
def connections(**kwargs):
    return render_template('admin/connections.html')


@blueprint.route('/shipping')
def shipping(**kwargs):
    shippings = Shipping.query.all()
    return render_template('admin/shipping.html', shippings=shippings)


@blueprint.route('/gallery')
def gallery(**kwargs):
    galleries = Gallery.query.all()
    return render_template('admin/gallery.html', galleries=galleries)


@blueprint.route('/gallery/add', methods=['GET', 'POST'])
def add_gallery(**kwargs):
    if request.method == 'POST':
        gallery = Gallery(name=request.form['name'])
        db.session.add(gallery)
        db.session.commit()
        imgs = request.files.getlist('userfile[]')
        gallery.upload_images(imgs)
        return redirect(url_for('admin.gallery'))
    return render_template('admin/add_gallery.html')


@blueprint.route('/gallery/edit/<int:id>', methods=['GET', 'POST'])
def edit_gallery(id):
    gallery = Gallery.query.get_or_404(int(id))

    if request.method == 'POST':
        print 'POST'
        if request.form['action'] == 'addPhotos':
            imgs = request.files.getlist('userfile[]')
            gallery.upload_images(imgs)
        elif request.form['action'] == 'changeTitle':
            gallery.name = request.form['name']
            db.session.add(gallery)
            db.session.commit()
        return redirect(url_for('admin.edit_gallery', id=gallery.id))
    return render_template('admin/edit_gallery.html', gallery=gallery)


@blueprint.route('/promotions')
def promotions(**kwargs):
    return render_template('admin/promotions.html')


@blueprint.route('/discounts')
def discounts(**kwargs):
    discounts = Discount.query.all()
    return render_template('admin/discounts.html', discounts=discounts)
