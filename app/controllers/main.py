# coding=utf-8
from flask import redirect
from flask import render_template
from flask import Blueprint
from flask.ext.login import logout_user

blueprint = Blueprint('main', __name__)


@blueprint.route('/')
def index(**kwargs):
    return render_template('index.html')


@blueprint.route('/logout')
def logout():
    logout_user()
    return redirect('main.index')


@blueprint.route('/gallery')
def gallery():
    return render_template('gallery.html')
