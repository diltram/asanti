# coding=utf-8
from flask import url_for
from flask import redirect
from flask import Blueprint
from flask import request
from flask.ext.login import current_user
from app import db
from app.models.param import Param
from app.models.gallery import Img
from app.models.gallery import Gallery
from app.models.product import Product
from app.models.category import Category
from app.models.shipping import Shipping
from app.models.discount import Discount

blueprint = Blueprint('admin_api', __name__)


# @blueprint.before_request
def restrict_blueprint_to_admins():
    if not current_user.is_authenticated() or not current_user.is_admin():
        return redirect('/login')  # request.url)


@blueprint.route('/categories', methods=['POST'])
def categories():
    actions = {
        'change': category_change,
        'add': category_add,
        'changeActive': category_activate,
        'delete': category_delete,
    }
    return actions.get(request.form['action'])()


def category_change():
    params = request.form
    cat = Category.query.get(int(params['categoryId']))
    cat.name = params['categoryName']
    db.session.add(cat)
    db.session.commit()
    return 'OK', 200


def category_add():
    params = request.form
    cat = Category(name=params['categoryName'],
                   parent_id=params['parentId'])
    db.session.add(cat)
    db.session.commit()
    return 'OK', 200


def category_activate():
    params = request.form
    cat = Category.query.get(int(params['categoryId']))
    states = {
        '1': True,
        '0': False
    }
    cat.active = states.get(params['active'])
    db.session.add(cat)
    db.session.commit()
    return 'OK', 200


def category_delete():
    params = request.form
    cat = Category.query.get(int(params['categoryId']))
    db.session.delete(cat)
    db.session.commit()
    return 'OK', 200


@blueprint.route('/params', methods=['POST'])
def params():
    print(request.form)
    actions = {
        'delete': param_delete,
        'addValue': param_add,
        'changeParam': param_edit,
        'addNewGroup': param_add_group,
    }
    return actions.get(request.form['action'])()


def param_delete():
    params = request.form
    param = Param.query.get(int(params['paramId']))
    db.session.delete(param)
    db.session.commit()
    return 'OK', 200


def param_add():
    params = request.form
    param = Param(name=params['value'], parent_id=params['paramOf'])
    db.session.add(param)
    db.session.commit()
    return 'OK', 200


def param_edit():
    params = request.form
    param = Param.query.get(int(params['id']))
    param.name = params['value']
    db.session.add(param)
    db.session.commit()
    return 'OK', 200


def param_add_group():
    params = request.form
    size_parent = Param.query.filter_by(name='sizes').first()
    param = Param(name=params['name'], parent=size_parent)
    param2 = Param(name=params['value'], parent=param)
    db.session.add(param, param2)
    db.session.commit()
    return 'OK', 200


@blueprint.route('/shipping', methods=['POST'])
def shipping():
    actions = {
        'update': shipping_change,
        'add': shipping_add,
        'changeActive': shipping_activate,
        'delete': shipping_delete,
    }
    return actions.get(request.form['action'])()


def shipping_change():
    params = request.form
    shipping = Shipping.query.get(int(params['shipId']))
    shipping.title = params['name']
    shipping.value = params['value']
    db.session.add(shipping)
    db.session.commit()
    return 'OK', 200


def shipping_add():
    params = request.form
    shipping = Shipping(title=params['newName'], value=params['newValue'])
    db.session.add(shipping)
    db.session.commit()
    return redirect(url_for('admin.shipping'))


def shipping_activate():
    params = request.form
    shipping = Shipping.query.get(int(params['shipId']))
    states = {
        '1': True,
        '0': False
    }
    shipping.is_active = states.get(params['active'], True)
    db.session.add(shipping)
    db.session.commit()
    return 'OK', 200


def shipping_delete():
    params = request.form
    shipping = Shipping.query.get(int(params['shipId']))
    db.session.delete(shipping)
    db.session.commit()
    return 'OK', 200


@blueprint.route('/discount', methods=['POST'])
def discount():
    actions = {
        'update': discount_change,
        'changeActive': discount_activate,
    }
    return actions.get(request.form['action'])()


def discount_change():
    params = request.form
    disc = Discount.query.get(int(params['discId']))
    disc.value = params['count']
    disc.discount = params['disc']
    db.session.add(disc)
    db.session.commit()
    return 'OK', 200


def discount_activate():
    params = request.form
    disc = Discount.query.get(int(params['discId']))
    states = {
        '1': True,
        '0': False
    }
    disc.is_active = states.get(params['active'], True)
    db.session.add(disc)
    db.session.commit()
    return 'OK', 200


@blueprint.route('/gallery', methods=['POST'])
def gallery():
    actions = {
        'delete': gallery_delete,
        'move': move_image,
        'deletePhoto': delete_photo,
    }
    return actions.get(request.form['action'])()


def gallery_delete():
    params = request.form
    gallery = Gallery.query.get_or_404(int(params['galleryId']))
    gallery.delete_images()
    db.session.delete(gallery)
    db.session.commit()
    return 'OK', 200


def move_image():
    params = request.form
    img = Img.query.get_or_404(int(params['photoId']))
    if params['direction'] == 'left':
        second_img = Img.query.filter(Img.parent_id == img.parent_id).filter(
            Img.order == img.order - 1).first()
    else:
        second_img = Img.query.filter(Img.parent_id == img.parent_id).filter(
            Img.order == img.order + 1).first()

    second_img.order, img.order = img.order, second_img.order
    db.session.add_all([img, second_img])
    db.session.commit()

    return 'OK', 200


def delete_photo():
    params = request.form
    img = Img.query.get_or_404(int(params['photoId']))
    Img.query.filter(Img.parent_id == img.parent_id).filter(
        Img.order > img.order).update({'order': Img.order - 1})

    img.remove_file()
    db.session.delete(img)
    db.session.commit()
    return 'OK', 200


@blueprint.route('/product', methods=['POST'])
def product():
    actions = {
        'changeActive': product_activate,
        'delete': product_delete,
    }
    return actions.get(request.form['action'])()


def product_activate():
    params = request.form
    prod = Product.query.get(int(params['productId']))
    states = {
        '1': True,
        '0': False
    }
    prod.is_active = states.get(params['active'])
    db.session.add(prod)
    db.session.commit()
    return 'OK', 200


def product_delete():
    params = request.form
    prod = Product.query.get(int(params['productId']))
    prod.delete_images()
    db.session.delete(prod)
    db.session.commit()
    return 'OK', 200
